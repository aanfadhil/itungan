import React, { Component } from 'react'
import { Switch } from 'react-router-dom'
import Route from 'react-router-dom/Route'

import IuranView from '../components/Iuran/IuranView'
class IuranController extends Component {


    
    render(){
        return(
            <Switch>
                <Route path="/iuran" exact component={IuranView}/>
            </Switch>
        )
    }
}

export default IuranController