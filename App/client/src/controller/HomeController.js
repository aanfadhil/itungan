import React, { Component } from 'react'
import { Switch } from 'react-router-dom'
import Route from 'react-router-dom/Route'

import HomeView from '../components/Home/HomeView'

class HomeController extends Component {
    render(){
        return(
            <Switch>
                <Route path="/home" component={HomeView}/>
                <Route path="/" exact component={HomeView}/>
            </Switch>
             


        )
    }
}

export default HomeController