import React, { Component } from 'react'
import { Switch } from 'react-router-dom'
import Route from 'react-router-dom/Route'

import BSSView from '../components/BayarSendiri/BSSView'
import BSSInputView from '../components/BayarSendiri/InputView'
import BSSBayarView from '../components/BayarSendiri/BSSBayarView'
import PembayaranSaya from '../components/BayarSendiri/PembayaranSaya'

class BSSController extends Component {


    
    render(){
        return(
            <Switch>
                <Route path="/bss" exact component={BSSView}/>
                <Route path="/bss/input" exact component={BSSInputView}/>
                <Route path="/bss/input/:id" exact component={BSSInputView}/>
                <Route path="/bss/:id" exact component={BSSBayarView}/>
                <Route path="/bss/bayar/:id" exact component={PembayaranSaya}/>
            </Switch>
        )
    }
}

export default BSSController