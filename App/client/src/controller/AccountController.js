import React, { Component } from 'react'
import { Switch } from 'react-router-dom'
import Route from 'react-router-dom/Route'

import LoginView from '../components/Account/LoginView'
import DaftarView from '../components/Account/DaftarView'

class HomeController extends Component {
    render(){
        return(
            <Switch>
                <Route path="/login" exact component={LoginView}/>
                <Route path="/register" exact component={DaftarView}/>
            </Switch>
            
        )
    }
}

export default HomeController
