import React, { Component } from 'react';
import { NavLink, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { getAuth } from '../actions/accountAction'
import queryString from 'query-string'

class FormLayout extends Component {

    constructor(props) {
        super(props)


        this.state = {
            backurl: ''
        }

    }

    componentWillMount() {
        this.props.getAuth();

        let redirectTo = this.props.backurl

        if (!redirectTo) {
            if (this.props.location.search) {
                let parsedq = queryString.parse(this.props.location.search)
                if (parsedq) {
                    if (parsedq.to) {
                        redirectTo = parsedq.to
                    }
                }
            }
        }

        if (redirectTo) {
            this.setState({ backurl: redirectTo })
        }


    }

    componentDidMount() {

    }

    goBack = (e) => {

        //console.log(this.props)

        if (!this.state.backurl) {
            window.history.back();
            e.preventDefault();
        }
    }

    render() {


        //
        let backlink = !this.state.backurl ? <a href="/" onClick={this.goBack} className="sidenav-trigger blue-text text-darken-2" style={{ display: "inherit" }}><i className="material-icons">arrow_back</i></a> : <NavLink to={this.state.backurl} className="sidenav-trigger blue-text text-darken-2" style={{ display: "inherit" }}><i className="material-icons">arrow_back</i></NavLink>

        return (
            <div className="layoutwrapper">
                <div className="navbar-fixed">
                    <nav>
                        <div className="nav-wrapper white blue-text text-darken-2">
                            {backlink}
                            <a href="#" style={{ fontSize: "1.5rem" }} className="brand-logo blue-text text-darken-2">{this.props.header}</a>
                            {this.props.util.isLoading ? (
                                <div className="progress nomargin" style={{ position: "absolute", bottom: "0px" }}>
                                    <div className="indeterminate"></div>
                                </div>
                            ) : null}
                        </div>

                    </nav>
                </div>
                <div className="content">
                    {this.props.children}
                </div>

            </div>

        )
    }
}
const mapStateToProps = (state) => ({
    account: state.account,
    util:state.util
})

export default connect(mapStateToProps, { getAuth })(withRouter(FormLayout))