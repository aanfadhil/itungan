import React, { Component } from 'react';
import { NavLink, withRouter } from 'react-router-dom';
import Layout from '../Layout'
import '../../style/views/bss.css'
import { getList } from '../../actions/bayarActions'
import { connect } from 'react-redux'
import { Common } from '../../Helper'

const $ = window.$

class IuranView extends Component {

    componentWillMount() {
        this.props.getList()
    }

    componentDidMount() {
        $('.collapsible').collapsible();
    }

    pembayaranbaru = (e) => {
        e.preventDefault()

        if (!this.props.account.token) {
            this.props.history.push('/login?to=/bss&s=/bss/input')
        }
        else {
            this.props.history.push('/bss/input')
        }

    }

    render() {
        return (
            <Layout header="Iuran">
                <div id="iuranwrapper">
                    <div className="row" style={{ marginTop: "3em" }}>
                        <div className="col s12">
                            <a href="/" to="/iuran/input" onClick={this.pembayaranbaru} className="btn fill">Buat Iuran Baru</a>
                        </div>
                    </div>

                    <div className="row center-align">
                        <div className="col s12">
                            <h6>Daftar Iuran</h6>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col s12">
                            <ul id="namaiuran" className="collapsible z-depth-0">

                                {this.props.bayar.bsses.map(item => (
                                    <li key={item._id}>
                                        <div className="collapsible-header"><i className="material-icons">access_time</i> {item.title}</div>
                                        <div className="collapsible-body" style={{ padding: "1rem 1rem 1rem 2rem" }}>
                                            <div className="row nomargin">
                                                <div className="col s12" style={{ fontSize: "0.9rem" }}>
                                                    <span>{Common.dateToString(item.createdat, 'HH:mm DD MMM YYYY')}</span>
                                                </div>
                                            </div>
                                            <div className="row nomargin" style={{ marginTop: "1rem" }}>
                                                <div className="col s4">
                                                    <span>Total</span>
                                                </div>
                                                <div className="col s8">
                                                    <span>: {Common.formatUang(item.total)}</span>
                                                </div>
                                            </div>
                                            <div className="row nomargin">
                                                <div className="col s4">
                                                    <span>Partisipan</span>
                                                </div>
                                                <div className="col s8">
                                                    <span>: {item.participantCount ||(item.participants || []).length}</span>
                                                </div>
                                            </div>

                                            <div className="row nomargin " style={{ marginTop: "1em" }}>
                                                <div className="col s12 right-align">
                                                    { (this.props.account.profile? this.props.account.profile.email:'') == item.owner ? (<NavLink to={"/bss/input/" + item._id} className="btn-small" style={{ marginRight: "5px" }}>Edit</NavLink>):null}
                                                    <NavLink to={"/bss/" + item._id} className="btn-small green">Bayar</NavLink>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                ))}
                            </ul>
                        </div>
                    </div>

                </div>
            </Layout>
        )
    }
}


const mapStateToProps = (state) => ({
    bayar: state.bayar,
    account: state.account
})

export default connect(mapStateToProps, { getList })(withRouter(IuranView))