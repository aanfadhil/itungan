import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
// import Route from 'react-router-dom/Route';

import Layout from '../Layout'
//import { Icon } from 'react-materialize';

import '../../style/views/home.css'

class HomeView extends Component {
    render() {
        return (
            <Layout header="Itungan">
                <div id="homewrapper">
                    <Feature 
                        title="Bayar Sendiri-sendiri"
                        body = "Lebih mudah hitung tagihan buat dibayar sendiri sendiri"
                        icon = {(<i className="material-icons feature-icon blue-text darken-1-text">monetization_on</i>)}
                        linkText = "MULAI"
                        url="/bss"
                    />
                    <Feature 
                        title="Iuran"
                        body = "Iuran lebih transparan, aman dan mudah"
                        icon = {(<i className="material-icons feature-icon blue-text darken-1-text">monetization_on</i>)}
                        linkText = "MULAI"
                        url="/login"
                    />
                </div>
            </Layout>
        )
    }
}


class Feature extends Component {
    render() {
        return (
            <div className="card feature">
                <div className="card-stacked">
                    <div className="card-content">
                        <div className="row">
                            <div className="col s12"><h5><strong>{this.props.title}</strong></h5></div>
                        </div>
                        <div className="row valign-wrapper">
                            <div className="col s8">
                                <p>{this.props.body}</p>
                            </div>
                            <div className="col s4">
                                {this.props.icon}
                            </div>

                        </div>
                    </div>
                    <div className="card-action">
                        <NavLink to={this.props.url}>{this.props.linkText}</NavLink>
                    </div>
                </div>
            </div>
        )
    }
}

export default HomeView