import React, { Component } from 'react'
import { NavLink, withRouter } from 'react-router-dom'
import FormLayout from '../FormLayout'
import { connect } from 'react-redux'
import Input from '../Utilities/Input'
import { Common } from '../../Helper'
import { register } from '../../actions/accountAction'
import queryString from 'query-string'

const M = window.M

class DaftarView extends Component {


    constructor() {
        super()
        this.state = {
            email: '',
            nama: '',
            pass: '',
            cnfpass: '',
            backurl:'/',
            cnfok: null,
            passwordvalid: null
        }
    }

    componentWillMount(){
        let redirectTo = this.props.redirect
        //console.log(this.props.location.search)
        if (!redirectTo) {
            if (this.props.location.search) {
                let parsedq = queryString.parse(this.props.location.search)
                if (parsedq) {
                    if (parsedq.s) {
                        redirectTo = parsedq.s
                    }
                }
            }
        }

        if (redirectTo) this.setState({ ...this.state, backurl: redirectTo })
    }

    handleChange = e => {
        e.preventDefault()
        let val = e.target.value;

        this.setState({
            [e.target.name]: val
        })

    }

    confirmPass = e => {
        e.preventDefault()
        let val = e.target.value;
        let passwordvalid = null

        if(e.target.name == 'pass'){
            if((val||'').length < 6 ){
                passwordvalid = false
            }
            else{
                passwordvalid = true
            }
            
        }

        this.setState({
            [e.target.name]: val
        },
            () => {
                

                if (this.state.pass || this.state.cnfpass) {
                    this.setState({
                        ...this.state,
                        cnfok: this.state.pass == this.state.cnfpass,
                        passwordvalid:passwordvalid == null?this.state.passwordvalid:passwordvalid
                    })
                }
                else {
                    this.setState({
                        ...this.state,
                        cnfok: null,
                        passwordvalid
                    })
                }
            }
        )


    }

    daftar = e =>{
        this.props.register({
            email:this.state.email,
            nama:this.state.nama,
            password:this.state.pass,
            passwordcnf:this.state.cnfpass
        },(res,err)=>{

            if(!res.errors){
                this.props.history.push(this.state.backurl||'/')
            }
            else{
                
                M.toast({ html: res.errors.map(e => e.msg).join("<br/>")+'<i class="material-icons red-text text-darken-1">error</i>', displayLength: 5000 })
            }
        })
    }

    render() {


        return (
            <FormLayout header="Daftar" backurl={"/login" + this.props.location.search}>
                <div id="logincontainer">
                    <div className="row nomargin" >
                        <Input s="12" label="Nama" type="text" id="nama" name="nama" value={this.state.nama} onChange={this.handleChange} />
                    </div>
                    <div className="row nomargin" >
                        <Input s="12" label="Email" errortext="email tidak valid" className="validate" type="email" id="email" name="email" value={this.state.email} onChange={this.handleChange} />
                    </div>
                    <div className="row nomargin" >
                        <Input s="12" isvalid={this.state.passwordvalid} helper={this.state.passwordvalid == false} errortext="minimal 6 karakter" label="Password" type="password" id="password" name="pass" value={this.state.pass} onChange={this.confirmPass} />
                    </div>
                    <div className="row nomargin" style={{ marginBottom: "0px" }}>
                        <Input s="12" isvalid={this.state.cnfok} helper={this.state.cnfok == false} errortext="password tidak sesuai" label="Confirm Password" type="password" id="cnfpassword" name="cnfpass" value={this.state.cnfpass} onChange={this.confirmPass} />
                    </div>
                    <div className="row">
                        <div className="col s12">
                            <button className="btn fill" onClick={this.daftar} disabled={!(this.state.cnfok && Common.validateEmail(this.state.email))} type="button">Daftar</button>
                        </div>
                    </div>
                </div>
            </FormLayout>
        )


    }


}


const mapStateToProps = (state) => ({
    account: state.account
})


export default connect(mapStateToProps, { register })(withRouter(DaftarView))