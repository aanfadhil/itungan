import React, { Component } from 'react';
import { NavLink, withRouter } from 'react-router-dom';
import firebase from "firebase/app"
import "firebase/auth"
import { googleLogin, login } from '../../actions/accountAction';
import { connect } from 'react-redux';
import StyledFirebaseAuth from "react-firebaseui/StyledFirebaseAuth"
// import Route from 'react-router-dom/Route';
import { Input, Button } from 'react-materialize'
import queryString from 'query-string'

import '../../style/views/login.css'
import FormLayout from '../FormLayout'

firebase.initializeApp({
    apiKey: "AIzaSyC1zj96C2OwUrTHMqYu9RbVXnAQRDj3B5U",
    authDomain: "itungansvc.firebaseapp.com"
})

const M = window.M

class LoginView extends Component {

    constructor(props) {
        super(props)

        this.googleLogin = this.props.googleLogin

        this.state = {
            isSignedIn: false,
            backurl: '/',
            showlogin: false,
            email:'',
            pass:''
        }
    }

    handleChange = e =>{
        this.setState({
            ...this.state,
            [e.target.name]:e.target.value
        })
    }

    componentWillMount() {
        let redirectTo = this.props.redirect
        //console.log(this.props.location.search)
        if (!redirectTo) {
            if (this.props.location.search) {
                let parsedq = queryString.parse(this.props.location.search)
                if (parsedq) {
                    if (parsedq.s) {
                        redirectTo = parsedq.s
                    }
                }
            }
        }

        if (redirectTo) this.setState({ ...this.state, backurl: redirectTo })
    }

    uiConfig = {
        signInFlow: "redirect",
        signInOptions: [
            firebase.auth.GoogleAuthProvider.PROVIDER_ID
        ],
        signInSuccessUrl: 'http://localhost:3000/google/auth',
        callbacks: {
            signInSuccessWithAuthResult: function (authResult, redirectUrl) {

                firebase.auth().currentUser.getIdToken(true).then(function (idToken) {
                    this.props.googleLogin(idToken, (user) => {
                        this.props.history.push(this.state.backurl)
                    })

                }.bind(this)).catch(function (error) {

                });
                return false;
            }.bind(this)
        }
    }

    componentDidMount = () => {
        firebase.auth().onAuthStateChanged(user => {
            this.setState({ isSignedIn: !!user })
        })
    }

    responseGoogle = (response) => {

    }

    toggleLogin = (e) => {
        e.preventDefault()
        this.setState({
            ...this.state,
            showlogin: !this.state.showlogin
        })
    }

    login = e =>{
        e.preventDefault()
        

        this.props.login({
            email:this.state.email,
            password:this.state.pass
        },(res)=>{
            if(res.status == 'success'){
                this.props.history.push(this.state.backurl)
            }
            else{
                M.toast({ html: res.error.message+'<i class="material-icons red-text text-darken-1">error</i>', displayLength: 5000 })
            }
        })

    }

    render() {

        return (
            <FormLayout header="Login">
                <div id="logincontainer">
                    {this.state.showlogin ? (
                        <div>
                            <div className="row" style={{ marginBottom: "0px" }}>
                                <Input s={12} label="Email" type="email" id="email" name="email" value={this.state.email} onChange={this.handleChange} />
                            </div>
                            <div className="row">
                                <Input s={12} label="Password" id="pass" type="password" name="pass" value={this.state.pass} onChange={this.handleChange} />
                            </div>
                            <div className="row">
                                <div className="col s6">
                                    <label>
                                        <input type="checkbox" className="filled-in" defaultChecked="checked" />
                                        <span>Ingat Saya</span>
                                    </label>
                                </div>
                                <div className="col s6 right-align" >
                                    <NavLink to="/register">Lupa Kata Sandi</NavLink>
                                </div>
                            </div>
                            <div className="row nomargin" style={{ marginBottom: "0.5em" }}>
                                <div className="col s12">
                                    <Button waves='light' onClick={this.login} style={{ width: "100%" }}>Masuk</Button>
                                </div>
                            </div>
                            <div className="row nomargin" style={{ marginBottom: "2em" }}>
                                <div className="col s12 center-align">
                                <NavLink to={"/register"+(this.props.location.search||'')}><strong>Daftar</strong></NavLink>
                                </div>
                            </div>
                        </div>
                    ) : null}
                    <div>
                        <div className="row nomargin" >
                            <div className="col s12 grey-text darken-5-text center-align">
                                Login Lebih Mudah Dengan
                            </div>
                        </div>
                        <div className="row nomargin">
                            <div className="col s12">
                                <StyledFirebaseAuth
                                    uiConfig={this.uiConfig}
                                    firebaseAuth={firebase.auth()}

                                />
                            </div>
                        </div>
                        {!this.state.showlogin ? (
                            <div>
                                <div className="row">
                                    <div className="col s12 grey-text darken-5-text center-align">
                                        atau
                                    <hr />
                                    </div>
                                </div>
                                <div className="row nomargin" style={{marginBottom:"1em"}}>
                                    <div className="col s12 grey-text darken-5-text center-align">
                                        <button className="btn fill" onClick={this.toggleLogin} style={{ maxWidth: "220px" }} type="button">Masuk</button>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col s12 center-align">
                                    <NavLink to={"/register"+(this.props.location.search||'')}><strong>Daftar</strong></NavLink>
                                    </div>
                                </div>
                            </div>
                        ) : null}
                    </div>


                </div>
            </FormLayout>
        )
    }
}
const mapStateToProps = (state) => ({
    account: state.account
})

export default connect(mapStateToProps, { googleLogin,login })(withRouter(LoginView));