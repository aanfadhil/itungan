import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { withRouter } from 'react-router-dom';

import { connect } from 'react-redux';
import FormLayout from '../FormLayout'
import { getById, bayar } from '../../actions/bayarActions';
import ModalUtil, { ModalClose } from '../Utilities/Modal'
import shortid from 'shortid'


import '../../style/views/bss.css'
import { Common } from '../../Helper';
import Decimal from 'decimal.js';
import Input from '../Utilities/Input';

const $ = window.$

class PembayaranSaya extends Component {

    constructor(props) {
        super(props)

        this.state = {
            inputItem: {
                _id: '',
                nama: '',
                jumlah: '',
                nilai: '',
                pajak: '',
                pajakchecked: false,
                setelahPajak: ''
            },
            total: '',
            pajakp: '',
            bersama: [],
            pajakrp: '',
            potongan: '',
            modal: null,
            test: '',
            title: '',
            participants: [],
            pembayaranSaya: {
                uangdikeluarkan: ''
            },
            fulllink: ''
        }
    }

    componentWillMount() {
        this.props.getById(this.props.match.params.id, (res) => {

            let data = res.data

            data.pajakp = data.pajakp || ''
            data.pajakrp = data.pajakrp || ''
            data.potongan = data.potongan || ''
            data.total = data.total || ''


            this.setState({
                ...this.state,
                ...data,
                pembayaranSaya: data.participants.filter(t => t.email == this.props.account.profile.email)[0] || {uangdikeluarkan: ''}
            })
        })
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.account.authChecked) {
            if (!this.props.account.token) {
                this.props.history.push('/login?to=/bss&s=/bss/bayar/' + this.props.match.params.id)
            }
        }
    }

    componentDidMount() {

        $('.collapsible').collapsible();
        $('.modal').modal({
            endingTop: '5vh',
            startingTop: '50vh'
        })
    }

    handleItemInputChange = (e) => {
        let value = e.target.value



        if (e.target.name == 'pajak' && this.state.inputItem.pajakchecked) {
            var val = ((new Decimal(this.state.inputItem.nilai)).times(value || 0).dividedBy(100)).add(this.state.inputItem.nilai).toDecimalPlaces(0).times(this.state.inputItem.jumlah)
            //val = val.round(val.toNumber()/100).times(100);
            //debugger;
            this.setState({
                ...this.state,
                inputItem: {
                    ...this.state.inputItem,
                    [e.target.name]: value,
                    setelahPajak: Math.round(val.toNumber()/100)*100 
                }
            })
        }
        else if (e.target.name == 'nilai' && this.state.inputItem.pajakchecked){
            var val = ((new Decimal(value)).times(this.state.inputItem.pajak || 0).dividedBy(100)).add(value).toDecimalPlaces(0).times(this.state.inputItem.jumlah)
            //val = val.round(val.dividedBy(100)).times(100);
            this.setState({
                ...this.state,
                inputItem: {
                    ...this.state.inputItem,
                    [e.target.name]: value,
                    setelahPajak: Math.round(val.toNumber()/100)*100 
                }
            })
        }
        else if (e.target.name == 'jumlah' && this.state.inputItem.pajakchecked){
            var val = ((new Decimal(this.state.inputItem.nilai)).times(this.state.inputItem.pajak || 0).dividedBy(100)).add(this.state.inputItem.nilai).toDecimalPlaces(0).times(value)
            //val = val.round(val.dividedBy(100)).times(100);
            this.setState({
                ...this.state,
                inputItem: {
                    ...this.state.inputItem,
                    [e.target.name]: value,
                    setelahPajak: Math.round(val.toNumber()/100)*100 
                }
            })
        }
        else {
            this.setState({
                ...this.state,
                inputItem: {
                    ...this.state.inputItem,
                    [e.target.name]: value
                }
            })
        }
    }

    pajakCheckedChange = e => {

        var value = e.target.checked


        this.setState({
            ...this.state,
            inputItem: {
                ...this.state.inputItem,
                pajakchecked: value,
                pajak: '',
                setelahPajak: ''
            }
        })
    }

    handleChange = (e) => {
        this.setState({
            ...this.state,
            pembayaranSaya: {
                ...this.state.pembayaranSaya,
                [e.target.name]: e.target.value
            }
        })
    }

    tambahItem = (e) => {
        e.preventDefault()

        // this.setState({
        //     ...this.state,
        //     inputItem: {
        //         ...this.state.inputItem,
        //         nama: '',
        //         jumlah: '',
        //         nilai: '',
        //         setelahPajak: '',
        //         pajak:'',
        //         pajakchecked:false
        //     }
        // })
        $('#popitem').modal('open')
        window.M.updateTextFields()

    }

    additemtolist = (e) => {
        e.preventDefault()

        this.setState({
            ...this.state,
            inputItem: {
                ...this.state.inputItem,
                nama: '',
                jumlah: '',
                nilai: '',
                setelahPajak: '',
                pajak:'',
                pajakchecked:false
            },
            pembayaranSaya: {
                ...this.state.pembayaranSaya,
                items: [{ ...this.state.inputItem, _id: shortid.generate() }, ...(this.state.pembayaranSaya.items || [])]

            }
        })
        window.M.updateTextFields()
        $('#popitem').modal('close')

    }

    confirmDelete = (id) => {

        this.setState({
            ...this.state,
            pembayaranSaya: {
                ...this.state.pembayaranSaya,
                items: (this.state.pembayaranSaya.items || []).filter(t => t._id !== id)
            }
        })
    }

    closeModal = e => {
        this.setState({
            ...this.state,
            inputItem: {
                ...this.state.inputItem,
                nama: '',
                jumlah: '',
                nilai: '',
                setelahPajak: ''
            }
        })
    }

    save = (e) => {
        e.preventDefault()
        //console.log(this.state.pembayaranSaya)
        this.props.bayar({ ...this.state.pembayaranSaya, _id: this.props.match.params.id }, (res) => {
            this.props.history.push("/bss/" + this.props.match.params.id)
        })

    }

    onAddedUangKeluar = added => {
        this.setState({...this.state,
            pembayaranSaya:{
                ...this.state.pembayaranSaya,
                uangdikeluarkan: parseFloat(this.state.pembayaranSaya.uangdikeluarkan) + added
            }
        })
    }

    render() {

        let totalBersama = (this.state.bersama || []).sum('nilai') || 0

        let bagirata = 0
        console.log(totalBersama)
        try {
            bagirata = (new Decimal(totalBersama)).dividedBy(this.state.participantCount).toNumber()
        } catch (er) {

        }

        let totalsaya = (this.state.pembayaranSaya.items || []).sum(t => ((new Decimal(t.nilai)).times(t.pajak||0).dividedBy(100)).add(t.nilai).times(t.jumlah))
        //  this.state.pajakp ? new Decimal((this.state.pembayaranSaya.items || []).sum(t => (new Decimal(t.nilai)).times(t.jumlah)) || 0).times(100 + this.state.pajakp).dividedBy(100)
        //     : 

        let harusbayar = new Decimal(totalsaya).plus(new Decimal(bagirata)).toNumber()

        let plusminus = this.state.pembayaranSaya.uangdikeluarkan - harusbayar
        let labelplusmin = plusminus > 0 ? "Lebih bayar" : "Kurang Bayar"

        return (
            <FormLayout header="Pembayaran Saya" backurl={"/bss/" + this.props.match.params.id}>

                <ModalUtil type="confirm" ref={(item) => { this.modal = item }}>Apakah anda yakin menghapus {this.state.todeletetext} ?</ModalUtil>

                <div id="bayarsayawrapper">

                    <div id="popitem" className="modal" style={{ overflowY: "visible" }}>
                        <div className="modal-content" style={{ paddingBottom: "3px" }}>
                            <ModalClose onClose={this.closeModal} popupid="popitem" />
                            <div className="row">
                                <div className="input-field col s12 m6">
                                    <input id="namabb" className="nomargin" type="text" value={this.state.inputItem.nama} name="nama" onChange={this.handleItemInputChange} />
                                    <label htmlFor="namabb">Nama Item</label>
                                </div>
                                <div className="input-field col s12 m6">
                                    <input id="nilaibb" type="number" name="nilai" value={this.state.inputItem.nilai} onChange={this.handleItemInputChange} />
                                    <label htmlFor="nilaibb">Nilai</label>
                                </div>
                                <div className="input-field col s12 m6">
                                    <input id="Jumlah" type="number" name="jumlah" value={this.state.inputItem.jumlah} onChange={this.handleItemInputChange} />
                                    <label htmlFor="Jumlah">Jumlah</label>
                                </div>
                                <div className="col s12 m6">
                                    <label>
                                        <input type="checkbox" className="filled-in" checked={this.state.inputItem.pajakchecked} onChange={this.pajakCheckedChange} />
                                        <span>Pajak</span>
                                    </label>
                                </div>
                                {(this.state.inputItem.pajakchecked ? (
                                    <div>
                                        <div className="input-field col s12 m6 ">
                                            <input id="pajak" type="number" className="" value={this.state.inputItem.pajak} name="pajak" onChange={this.handleItemInputChange} />
                                            <label htmlFor="pajak">Pajak</label>
                                        </div>
                                        <div className="input-field col s12 m6 nomargin">
                                            <input id="setelahPajak" type="number" className="nomargin" value={this.state.inputItem.setelahPajak} name="setelahPajak" disabled />
                                            <label htmlFor="setelahPajak" className={this.state.inputItem.setelahPajak ? "active" : null}>Setelah Pajak</label>
                                        </div>
                                    </div>
                                ) : null)}


                            </div>
                        </div>
                        <div className="modal-footer center-align" style={{ paddingTop: "2px" }}>
                            <div className="row nomargin">
                                <div className="col s12 nomargin">
                                    <button className="modal-close nomargin fill waves-effect waves-green btn" onClick={this.additemtolist} >Tambah</button>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div className="row nomargin" style={{ marginTop: "2em" }}>
                        <Input s="12" label="Bayar Bersama" containerclass="nomargin" disabled={true} id="bayarbersama" type="text" value={Common.formatUang(bagirata) + " @ orang"} />
                    </div>
                    <div className="row nomargin" style={{ marginTop: "1em" }}>
                        <Input s="12" label="Harus Dibayar (Bayar Bersama + Total Pribadi)" containerclass="nomargin" disabled={true} id="pengeluaransaya" type="text"
                            value={Common.formatUang(harusbayar)}
                             />
                    </div>
                    <div className="row" style={{ marginTop: "1em" }}>
                        <Input s="12" label="Uang Dikeluarkan" useaddbtn={true} onAdded={this.onAddedUangKeluar} id="uangkeluar" type="number" value={this.state.pembayaranSaya.uangdikeluarkan} onChange={this.handleChange} name="uangdikeluarkan" />
                    </div>
                    {plusminus != 0 ? (
                        <div className="row nomargin" style={{ marginTop: "1em" }}>
                            <Input s="12" label={labelplusmin} containerclass="nomargin" disabled={true} id="pengeluaransaya" type="text"
                                value={Common.formatUang(plusminus)} />
                        </div>
                    ) : null}

                    <div className="row">
                        <div className="col s12">
                            <button className="btn waves-effect waves-light " onClick={this.tambahItem}><i className="material-icons left">add</i>Tambah Item Saya</button>
                        </div>
                    </div>
                    <div className="row nomargin">
                        <div className="col s6">
                            <span>Total Pribadi</span>
                        </div>
                        <div className="col s6">
                            <span>: {
                                Common.formatUang(totalsaya)
                                    //(this.state.pembayaranSaya.items || []).sum(t => (new Decimal(t.nilai)).times(t.jumlah)))
                                }
                                </span>
                        </div>
                    </div>
                    {this.state.pajakp ? (
                        <div className="row nomargin">
                            <div className="col s6">
                                <span>Total Pribadi + Pajak ({this.state.pajakp}%)</span>
                            </div>
                            <div className="col s6">

                                <span>: {Common.formatUang(totalsaya)
                                 //new Decimal((this.state.pembayaranSaya.items || []).sum(t => (new Decimal(t.nilai)).times(t.pajak||0).dividedBy(100).add(t.nilai).times(t.jumlah)) || 0))}</span>
                                 }
                                 </span>
                            </div>
                        </div>
                    ) : null}

                    <div className="row">
                        <div className="col s12">

                            <ul id="namabayar" className="collapsible z-depth-0">
                                {(this.state.pembayaranSaya.items || []).map(item => (

                                    <li key={item._id}>
                                        <div className="collapsible-header"><span><strong>{item.nama}</strong> - {Common.formatUang(((new Decimal(item.nilai)).times(item.pajak||0).dividedBy(100)).add(item.nilai).times(item.jumlah).toNumber())}</span>  <i className="material-icons deletebtn red-text right" onClick={this.confirmDelete.bind(null, item._id)}>close</i></div>
                                        <div className="collapsible-body"><span>{"@ " + Common.formatUang(item.nilai)} {" x " + item.jumlah} {item.pajak?`+ pajak  ${item.pajak}%`:""}</span></div>
                                    </li>
                                ))}
                            </ul>

                        </div>
                    </div>
                    <div className="row">
                        <div className="col s12">
                            <button onClick={this.save} className="btn fill green">SIMPAN</button>
                        </div>
                    </div>
                </div>


            </FormLayout>
        )
    }
}

const mapStateToProps = (state) => ({
    detailbayar: state.bayar,
    account: state.account

})

export default connect(mapStateToProps, { getById, bayar })(withRouter(PembayaranSaya))