import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { withRouter } from 'react-router-dom';
import { Decimal } from 'decimal.js'
import { connect } from 'react-redux';
import FormLayout from '../FormLayout'
import { getBayarData } from '../../actions/bayarActions';
import { Common } from '../../Helper'
import { ModalClose } from '../Utilities/Modal'

import '../../style/views/bss.css'

const $ = window.$

class BSSBayarView extends Component {


    componentWillMount() {

        this.props.getById(this.props.match.params.id, (err, result) => { })
    }

    componentDidMount() {

        $('.collapsible').collapsible();
        $('.modal').modal({
            endingTop: '5vh',
            startingTop: '50vh'
        })

    }

    listbersama = (e) => {
        e.preventDefault();

        $("#popitem").modal('open')

    }

    info = e =>{
        e.preventDefault()
        
        $('#popinfo').modal("open")

    }

    closeModal = e => {
        e.preventDefault();

        $(".modal").modal('close')
    }

    render() {

        let totalBersama = (this.props.bayar.bersama || []).sum('nilai') || 0

        let bagirata = 0

        let diklaim = 0;

        if (this.props.bayar.participants) {

            this.props.bayar.participants.forEach(item => {
                diklaim += (item.items || []).sum(t => ((new Decimal(t.nilai)).times(t.pajak || 0).dividedBy(100)).add(t.nilai).times(t.jumlah))
            });
        }

        let belumdiklaim = this.props.bayar.total > 0 ? this.props.bayar.total - (diklaim + totalBersama) : 0;

        console.log("diklaim",diklaim)

        try {
            bagirata = (new Decimal(totalBersama)).dividedBy((this.props.bayar.participantCount || 1)).toNumber()
        } catch (er) {

        }

        return (
            <FormLayout header={"Pembayaran"} backurl={"/bss"}>



                <div id="bsswrapper">
                    <div className="row nomargin">
                        <div className="col s12">
                            <h5><strong>{this.props.bayar.title}</strong></h5>
                        </div>
                    </div>
                    {this.props.bayar.struk ? (

                        <div className="row nomargin">
                            <div className="col s12 nomargin nopadding">
                                <a href={"https://itungan.com/api/bss/downloadstruk/" + this.props.match.params.id} style={{ lineHeight: "3rem", verticalAlign: "bottom" }}><i className="material-icons" style={{ lineHeight: "3rem" }}>file_download</i>Download Struk</a>
                            </div>
                        </div>
                    ) : null}
                    <div className="row left-align nomargin">
                        <div className="col s4">
                            <h6>Total</h6>
                        </div>
                        <div className="col s8  ">
                            <h6>: {Common.formatUang(this.props.bayar.total)}</h6>
                        </div>
                    </div>


                    <div id="popinfo" className="modal" style={{ overflowY: "visible" }}>

                        <div className="modal-content" style={{ padding: "5px",paddingTop:"1rem" }}>
                            <div className="row">
                                <div className="col s12">
                                    <h5><i className="material-icons green-text text-lighten-2" style={{fontSize:"2.2rem",lineHeight:"3rem",verticalAlign:"sub"}}>help_outline</i>Belum Diklaim</h5>
                                    Adalah milai selain dibayar bersama yang belum dimasukan ke item pribadi oleh peserta<br/>
                                    <br/>
                                    <strong>Rumus:</strong>
                                    <br/>
                                    Total - (Total Dibayar Bersama + Jumlah item pribadi semua peserta)
                                </div>
                            </div>
                        </div>
                    </div>

                    {this.props.bayar.total > 0 ? (
                        <div className="row left-align nomargin">
                            <div className="col s4">
                                <h6>Belum Diklaim</h6>
                            </div>
                            <div className="col s8  ">
                                <h6>: {Common.formatUang(belumdiklaim)}<a><i className="material-icons green-text text-lighten-2" onClick={this.info} style={{marginLeft:"5px",fontSize:"1.15rem",lineHeight:"110%",verticalAlign:"bottom"}}>help_outline</i></a></h6>

                            </div>
                        </div>
                    )
                        : null
                    }

                    {this.props.bayar.pajakp ? (
                        <div className="row left-align nomargin">
                            <div className="col s4">
                                <h6>Pajak (%)</h6>
                            </div>
                            <div className="col s8  ">
                                <h6>: {this.props.bayar.pajakp}%</h6>
                            </div>
                        </div>
                    ) : null}
                    {this.props.bayar.pajakp ? (
                        <div className="row left-align nomargin">
                            <div className="col s4">
                                <h6>Pajak (Rp)</h6>
                            </div>
                            <div className="col s8  ">
                                <h6>: {Common.formatUang(this.props.bayar.pajakrp)}</h6>
                            </div>
                        </div>

                    ) : null}

                    <div className="row left-align nomargin">
                        <div className="col s4">
                            <h6>Peserta</h6>
                        </div>
                        <div className="col s8  ">
                            <h6>: {this.props.bayar.participantCount} Orang</h6>
                        </div>
                    </div>


                    <div className="row left-align nomargin">
                        <div className="col s4">
                            <h6>Dibayar bersama</h6>
                        </div>
                        <div className="col s8">
                            <h6>: <a href="/" onClick={this.listbersama}> {Common.formatUang(totalBersama)}</a></h6>
                        </div>
                    </div>

                    <div id="popitem" className="modal" style={{ overflowY: "visible" }}>
                        <div className="modal-content" style={{ padding: "5px" }}>
                            <div className="row nomargin">
                                <div className="col s12 nomargin nopadding">
                                    <h6><strong>Dibayar Bersama</strong></h6>
                                </div>
                            </div>
                            <div className="row nomargin" style={{ marginBottom: "5px" }}>
                                <div className="col s12 nomargin nopadding">
                                    {Common.formatUang(bagirata)} @ orang
                                </div>
                            </div>
                            <ModalClose onClose={this.closeModal} popupid="popitem" />
                            <div className="row nomargin">
                                <div className="col s12 nomargin " style={{ padding: "0px" }}>
                                    <ul className="collection nomargin" style={{ marginBottom: "0px", boxShadow: "none" }}>
                                        {(this.props.bayar.bersama || []).map(item => (
                                            <li key={item._id} className="collection-item"><div>{item.title} - <strong>{Common.formatUang(item.nilai)}</strong></div></li>
                                        ))}

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div className="row" style={{ marginTop: "1em" }}>
                        <div className="col s12">
                            <NavLink className="btn" to={"/bss/bayar/" + this.props.match.params.id}><i className="material-icons left">edit</i>input pembayaran saya</NavLink>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col s12">

                            <ul id="namabayar" className="collapsible z-depth-0">
                                {(this.props.bayar.participants || []).map(item => {

                                    let result = 0;

                                    let totalPribadi = (item.items || []).sum(t => ((new Decimal(t.nilai)).times(t.pajak || 0).dividedBy(100)).add(t.nilai).times(t.jumlah))
                                    // this.props.bayar.pajakp ?
                                    //     new Decimal((item.items || []).sum(t => (new Decimal(t.nilai)).times(t.jumlah)) || 0).times(100 + this.props.bayar.pajakp).dividedBy(100)
                                    //     : (item.items || []).sum(t => (new Decimal(t.nilai)).times(t.jumlah))

                                    let hasilpribadi = new Decimal(item.uangdikeluarkan || 0).minus(totalPribadi)
                                    let hasil = hasilpribadi.minus(bagirata).toNumber()

                                    hasilpribadi = hasilpribadi.toNumber()


                                    let display = (<span></span>)

                                    if (hasil > 0) {
                                        display = (<span className="green-text text-darken-2"> (+{Common.formatUang(hasil)})</span>)
                                    }
                                    else if (hasil < 0) {
                                        display = (<span className="red-text text-darken-1"> (-{Common.formatUang(Math.abs(hasil))})</span>)
                                    }

                                    return (

                                        <li key={item._id}>
                                            <div className="collapsible-header"><strong>{item.nama}</strong>{display/*Common.formatUang(new Decimal((item.items || []).sum(t => (new Decimal(t.nilai)).times(t.jumlah)) || 0).times(100 + this.props.bayar.pajakp).dividedBy(100))*/}</div>
                                            <div className="collapsible-body">
                                                <div className="row nomargin">
                                                    <div className="row nomargin">
                                                        <div className="col s4">
                                                            Uang Keluar
                                                    </div>
                                                        <div className="col s4">
                                                            : {Common.formatUang(item.uangdikeluarkan)}
                                                        </div>
                                                    </div>
                                                    <div className="row nomargin">
                                                        <div className="col s4">
                                                            Total {(this.props.bayar.pajakp ? '+ pajak' : '')}
                                                        </div>
                                                        <div className="col s4">
                                                            : {Common.formatUang(totalPribadi)}
                                                        </div>
                                                    </div>
                                                    <ol>
                                                        {(item.items || []).map(t => (
                                                            <li key={t._id} className="col nomargin s12">{t.nama} x{t.jumlah} @{Common.formatUang(t.nilai)} {t.pajak ? `+ pajak ${t.pajak}%` : ""} </li>
                                                        ))}
                                                    </ol>

                                                </div>
                                            </div>
                                        </li>
                                    )
                                })}

                            </ul>

                        </div>
                    </div>

                </div>
            </FormLayout>
        )
    }
}

const mapStateToProps = (state) => ({
    bayar: state.bayar.bayar
})

export default connect(mapStateToProps, { getById: getBayarData })(withRouter(BSSBayarView));