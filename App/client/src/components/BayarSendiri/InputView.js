import React, { Component } from 'react';
import { Decimal } from 'decimal.js'
import FormLayout from '../FormLayout'
import { getById, save, uploadImage } from '../../actions/bayarActions'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { NavLink } from 'react-router-dom'
import '../../style/views/bss.css'

import { Common } from '../../Helper';
import shortid from 'shortid'
import Input from '../Utilities/Input';



const $ = window.$;
const M = window.M
//const M = window.M;
class BSSInputView extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isnew: true,
            total: '',
            pajakp: '',
            bersama: [],
            pajakrp: '',
            potongan: '',
            modal: null,
            test: '',
            participantCount: '',
            title: '',
            fulllink: '',
            inputBersama: {
                _id: '',
                nilai: '',
                title: '',
                pajak: '',
                setelahPajak: ''
            },
            struk: null
        }

    }


    componentWillMount() {

        if (this.props.match.params.id) {

            this.props.getById(this.props.match.params.id, (res) => {
                console.log(res)
                let data = res.data

                data.pajakp = data.pajakp || ''
                data.pajakrp = data.pajakrp || ''
                data.potongan = data.potongan || ''
                data.total = data.total || ''

                this.setState({
                    ...this.state,
                    ...data,
                    isnew: false
                })
            })

        }
    }

    componentDidMount() {

        $('.modal').modal({
            endingTop: '5vh',
            startingTop: '50vh'
        })


    }

    copy = (e) => {
        e.preventDefault();
    }

    totalChange = (e) => {
        let value = e.target.value;
        this.setState({
            ...this.state,
            total: value
        })
    }

    pajakpChange = (e) => {
        let value = e.target.value;

        if (value) {
            value = new Decimal(value);
            var total = new Decimal(this.state.total || 0);

            var pajakrp = total.minus(total.dividedBy(value.dividedBy(100).plus(1))).toDecimalPlaces(0)
        }

        this.setState({
            ...this.state,
            pajakp: value,
            pajakrp: value ? pajakrp : ''
        })
    }

    pajakrpChange = (e) => {
        let value = e.target.value;
        value = new Decimal(value);
        var total = new Decimal(this.state.total || 0);

        if (value) {
            var pajakp = value.dividedBy(total.minus(value)).times(100).toDecimalPlaces(0)
        }

        this.setState({
            ...this.state,
            pajakp: pajakp >= 0 && value ? pajakp : '',
            pajakrp: value
        })
    }

    onTotalAdded = valueadded =>{
        
        this.setState({
            ...this.state,
            total : this.state.total + valueadded
        })
    }

    tambahBersama = (e) => {
        e.preventDefault()
        this.setState({
            ...this.state,
            inputBersama: {
                nilai: '',
                title: '',
                setelahPajak: ''
            }
        })
        $('#modal1').modal('open')

    }

    simpanBersama = (e) => {
        e.preventDefault()

        this.setState({
            ...this.state,
            bersama: [...this.state.bersama, { ...this.state.inputBersama, _id: shortid.generate() }],
            inputBersama: {
                nilai: '',
                title: '',
                setelahPajak: ''
            }
        })

        $('#modal1').modal('close')
    }

    handleChange = (e) => {
        let value = e.target.value

        this.setState({
            [e.target.name]: value
        })
    }

    handlenumberChange = (e) => {
        let value = e.target.value

        this.setState({
            [e.target.name]: value
        })

    }

    handleBersamaChange = (e) => {
        let value = e.target.value

        this.setState({
            ...this.state,
            inputBersama: {
                ...this.state.inputBersama,
                [e.target.name]: value
            }
        })

    }

    fileSelected = e => {
        if (e.target.files.length > 0) {
            this.setState({ ...this.state, struk: e.target.files[0] })
        }
        else {
            this.setState({ ...this.state, struk: null })
        }
    }

    closemodal = (e) => {
        e.preventDefault()
        $('#modal1').modal('close')
    }

    deleteBersama = (id) => {
        this.setState({
            ...this.state,
            bersama: this.state.bersama.filter(t => t._id !== id)
        })
    }


    save = (e) => {
        e.preventDefault()

        let data = {
            ...this.state,
            bersama: this.state.bersama.map(t => { return { nilai: t.nilai, title: t.title } }),
            inputBersama: undefined,
            pajakp: this.state.pajakp || null,
            pajakrp: this.state.pajakrp || null,
            potongan: this.state.potongan || null,
            total: this.state.total || null,
            struk: undefined
        }

        this.props.save(data, (res) => {

            if (res.status == 201) {

                if (this.state.struk) {

                    let formData = new FormData();
                    formData.append('photos[]', this.state.struk, this.state.struk.name);
                    this.props.uploadImage(formData, res.data._id, (upres) => {
                        this.saveDone(upres.data._id)
                    })
                }
                else {
                    this.saveDone(res.data._id)
                }

            }
        })

    }


    saveDone = _id => {
        this.props.history.push('/bss/input/' + _id)

        this.props.getById(this.props.match.params.id, (res) => {

            let data = res.data

            data.pajakp = data.pajakp || ''
            data.pajakrp = data.pajakrp || ''
            data.potongan = data.potongan || ''
            data.total = data.total || ''

            this.setState({
                ...this.state,
                ...data,
                isnew: false
            })

            window.scrollTo(0, 0);

            M.toast({ html: 'Berhasil<i class="material-icons green-text text-darken-1">check_circle</i>', displayLength: 1500 })

        })
    }


    onShare = e => {
        e.preventDefault()
        console.log("onshare")
        if (navigator.share) {
            navigator.share({
                title: 'Itungan.com',
                text: this.state.title,
                url: this.state.fulllink,
            })
                .then(() => console.log('Successful share'))
                .catch((error) => console.log('Error sharing', error));
        }
    }

    render() {
        let header;
        if (!this.state.isnew) {
            header = (
                <div>
                    <div className="row nomargin">
                        <div className="col s12">
                            Link Pembayaran
                        </div>
                    </div>
                    <div className="row nomargin">
                        <div className="col s12" style={{ lineHeight: "2em" }}>
                            <NavLink style={{ lineHeight: "1.2rem", verticalAlign: "middle" }} to={this.state.link}>{this.state.fulllink}</NavLink>
                        </div>
                    </div>

                    <div className="row nomargin" style={{ marginTop: "1em" }}>
                        <div className="col s12">
                            <button onClick={this.onShare} className="waves-effect waves-teal btn-small fill" style={{ paddingLeft: "2px", paddingRight: "2px" }}>
                                Bagikan Link
                            </button>
                        </div>
                    </div>

                    <div className="row" style={{ marginTop: "1em" }}>
                        <div className="col s12">
                            <button className="btn fill green">Tutup Pembayaran</button>
                        </div>
                    </div>
                </div>

            )
        }

        return (
            <FormLayout header="Pembayaran" backurl="/bss">
                <div id="inputbsswrapper">

                    {header}

                    <div className="row">
                        <div className="input-field col s12 m4">
                            <input id="title" type="text" value={this.state.title} name="title" onChange={this.handleChange} />
                            <label htmlFor="title" className={this.state.title ? "active" : null}>Nama Pembaayaran</label>
                        </div>
                        <div className="input-field col s12 m4">
                            <input id="total" type="number" value={this.state.participantCount} name="participantCount" onChange={this.handleChange} />
                            <label htmlFor="total" className={this.state.participantCount ? "active" : null}>Jumlah Peserta</label>
                        </div>
                        {/* <div className="input-field col s12 m4">
                            <input id="total" type="number" value={this.state.total} name="total" onChange={this.totalChange} />
                            <label htmlFor="total" className={this.state.total ? "active" : null}>Total Bayar</label>
                            
                        </div> */}
                        <Input s="12" label="Total Bayar" id="total" type="number"
                            value={this.state.total}
                            onChange={this.totalChange} name="total" useaddbtn={true} onAdded={this.onTotalAdded} />
                        

                        <div className="input-field col s12 m4">
                            <input id="potongan" type="number" value={this.state.potongan} name="potongan" onChange={this.handleChange} />
                            <label htmlFor="potongan" className={this.state.potongan ? "active" : null}>Potongan</label>
                        </div>

                        <div className="input-field col s6 hidden m4">
                            <input id="pajakp" type="number" value={this.state.pajakp} name="pajakp" onChange={this.pajakpChange} />
                            <label htmlFor="pajakp" className={this.state.pajakp ? "active" : null}>Pajak (%)</label>
                        </div>

                        <div className="input-field col s6 hidden m4">
                            <input id="pajakrp" type="number" value={this.state.pajakrp} name="pajakrp" onChange={this.pajakrpChange} />
                            <label htmlFor="pajakrp" className={this.state.pajakrp ? "active" : null}>Pajak (Rp)</label>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col s12">
                            <div className="file-field input-field">
                                <div className="btn">
                                    <span>File</span>
                                    <input type="file" accept="image/*;capture=camera" onChange={this.fileSelected} />
                                </div>
                                <div className="file-path-wrapper">
                                    <input className="file-path validate" type="text" placeholder="Foto Struk" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="row" style={{ marginTop: "3em" }}>
                        <div className="col s12">
                            <button className="btn waves-effect waves-light btn" type="button" onClick={this.tambahBersama}><i className="material-icons left">add</i>Dibayar bersama</button>
                        </div>
                    </div>

                    <div id="modal1" className="modal">
                        <div className="modal-content">

                            <i className="material-icons grey-text" style={{ position: "absolute", top: "7px", right: "7px", fontSize: "1.2rem" }} onClick={this.closemodal}>close</i>
                            <div className="row">
                                <div className="input-field col s12 m6">
                                    <input id="namabb" className="nomargin" type="text" value={this.state.inputBersama.title} name="title" onChange={this.handleBersamaChange} />
                                    <label htmlFor="namabb" className={this.state.inputBersama.title ? "active" : null}>Nama Item</label>
                                </div>
                                <div className="input-field col s12 m6 nomargin">
                                    <input id="nilaibb" type="number" value={this.state.inputBersama.nilai} name="nilai" onChange={this.handleBersamaChange} />
                                    <label htmlFor="nilaibb" className={this.state.inputBersama.nilai ? "active" : null}>Nilai Sebelum Pajak</label>
                                </div>
                            </div>
                        </div>
                        <div className="modal-footer center-align">
                            <div className="row nomargin">
                                <div className="col s12 nomargin">
                                    <button className="modal-close fill waves-effect waves-green btn" onClick={this.simpanBersama}>Tambah</button>
                                </div>
                            </div>

                        </div>
                    </div>

                    

                    <div id="modal1" className="modal">
                        <div className="modal-content">

                            <i className="material-icons grey-text" style={{ position: "absolute", top: "7px", right: "7px", fontSize: "1.2rem" }} onClick={this.closemodal}>close</i>
                            <div className="row">
                                <div className="input-field col s12 m6">
                                    <input id="namabb" className="nomargin" type="text" value={this.state.inputBersama.title} name="title" onChange={this.handleBersamaChange} />
                                    <label htmlFor="namabb" className={this.state.inputBersama.title ? "active" : null}>Nama Item</label>
                                </div>
                                <div className="input-field col s12 m6 nomargin">
                                    <input id="nilaibb" type="number" value={this.state.inputBersama.nilai} name="nilai" onChange={this.handleBersamaChange} />
                                    <label htmlFor="nilaibb" className={this.state.inputBersama.nilai ? "active" : null}>Nilai Sebelum Pajak</label>
                                </div>
                            </div>
                        </div>
                        <div className="modal-footer center-align">
                            <div className="row nomargin">
                                <div className="col s12 nomargin">
                                    <button className="modal-close fill waves-effect waves-green btn" onClick={this.simpanBersama}>Tambah</button>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div className="row" style={{ marginBottom: "0px" }}>
                        <div className="col s12">
                            <ul className="collection" style={{ marginBottom: "0px", boxShadow: "none" }}>
                                {this.state.bersama.map(item => (
                                    <li key={item._id} className="collection-item"><div>{item.title + " - " + Common.formatUang(item.nilai)}<a href="#!" className="secondary-content red-text"><i className="material-icons" onClick={this.deleteBersama.bind(null, item._id)}>close</i></a></div></li>
                                ))}
                            </ul>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col s6 ">
                            Total dibayar bersama
                        </div>
                        <div className="col s6 right-align">
                            {Common.formatUang(this.state.bersama.sum('nilai'))}
                        </div>
                    </div>

                    <div className="row" style={{ marginTop: "2em" }}>
                        <div className="col s12">
                            <button className="btn fill" style={{ height: "4em" }} onClick={this.save}>SIMPAN</button>
                        </div>
                    </div>

                </div>
            </FormLayout>
        )
    }
}

const mapStateToProps = (state) => ({
    bayar: state.bayar,
    account: state.account
})
export default connect(mapStateToProps, { getById, save, uploadImage })(withRouter(BSSInputView))
