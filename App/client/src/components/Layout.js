import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { getAuth, logout } from '../actions/accountAction'
import '../style/views/layout.css'
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom'

class Layout extends Component {

    constructor() {
        super();

        this.state = {
            openNav: false
        }
    }

    componentWillMount() {
        this.props.getAuth();

    }

    open = (e) => {
        this.setState({ openNav: true })
        //e.stopPropagation();
        //e.nativeEvent.stopImmediatePropagation();
        e.preventDefault();
    }

    close = () => {
        this.setState({ openNav: false })
    }

    logout = (e) => {
        e.preventDefault()

        this.props.logout()

        this.props.history.push('/')
    }

    render() {

        let account = (this.props.account.token) ? (
            <div>
                <a href="#user" style={{marginBottom:"10px"}}>
                    {this.props.account.profile.profilePict?(
                        <img className="circle" src={this.props.account.profile.profilePict} alt="" />
                    ):(
                        <div className="profalt">{this.props.account.profile.fullName[0].toUpperCase()}</div>
                    )}
                </a>
                <a href="#name" onClick={e => e.preventDefault() }><span className="white-text name">{this.props.account.profile.fullName}</span></a>
                <a href="#email" onClick={e => e.preventDefault() }><span className="white-text email">{this.props.account.profile.email}</span></a>
            </div>
        ) : null

        let accountButton = (!this.props.account.token) ? (
            <li onClick={this.close}><NavLink to="/login"><i className="material-icons">account_circle</i>Login</NavLink></li>
        )
            : (
                <li onClick={this.close}><a href="/" onClick={this.logout}><i className="material-icons">account_circle</i>Logout</a></li>
            )

        return (
            <div className="layoutwrapper" >
                <div className={(this.state.openNav ? "sidebaroverlayopen" : "sidebar-overlay")} onClick={this.close}></div>
                <div className="navbar-fixed">
                    <nav>
                        <div className="nav-wrapper white blue-text text-darken-2">

                            <a href="/" onClick={this.open} className="sidenav-trigger blue-text text-darken-2" style={{ display: "block" }} >
                                <i className="material-icons">menu</i>
                            </a>
                            <a className="brand-logo blue-text text-darken-2" style={{ fontSize: "130%" }}>{this.props.header}</a>
                            {this.props.util.isLoading ? (
                                <div className="progress nomargin" style={{ position: "absolute", bottom: "0px" }}>
                                    <div className="indeterminate"></div>
                                </div>
                            ) : null}


                        </div>
                    </nav>
                </div>

                <ul id="mySidenav" style={{ marginTop: "0px" }} className={"sidenav " + (this.state.openNav ? "sidenavopen z-depth-3 " : "")}>
                    <li>
                        <div className="user-view">
                            <div className="sidenav-backgroud">
                                <img src={"https://picsum.photos/1000?image=1067&gravity=north&blur"} style={{objectFit:"cover",width:"100%",height:"100%",objectPosition:"center"}} alt="" />
                            </div>
                            {account}
                        </div>
                    </li>
                    {accountButton}
                    <li onClick={this.close}><div className="divider"></div></li>
                    <li onClick={this.close}><NavLink to="/"><i className="material-icons">home</i>Home</NavLink></li>
                    <li onClick={this.close}><NavLink to="/bss"><i className="material-icons">monetization_on</i>Bayar Sendiri-sendiri</NavLink></li>
                    <li onClick={this.close}><NavLink to="/bss"><i className="material-icons">monetization_on</i>Iuran</NavLink></li>
                    <li onClick={this.close}><NavLink to="/bss"><i className="material-icons">settings</i>Settings</NavLink></li>
                </ul>


                <div className="content">
                    {this.props.children}
                </div>

            </div>

        )
    }
}
const mapStateToProps = (state) => ({
    account: state.account,
    util: state.util
})

export default connect(mapStateToProps, { getAuth, logout })(withRouter(Layout))
