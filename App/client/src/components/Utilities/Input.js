import React, { Component } from 'react';
import { Common } from '../../Helper';
const $ = window.$

class Input extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isInvalid: false,
            add: ''
        }
    }

    // setvalidation = (validation) => {
    //     if(typeof(validation) === "boolean")
    //         this.setState({isInvalid:validation})
    // }

    validateInput = (event) => {
        if (this.props.type == "number") {
            if (event.which != 8 && isNaN(String.fromCharCode(event.which))) {
                event.preventDefault(); //stop character from entering input
            }
        }
    }

    addTotal = null;

    onAdd = e => {
        e.preventDefault()
        this.props.onAdded(parseFloat(this.state.add))
        this.setState({
            ...this.state,
            add: ''
        })
        window.$("#addmodal" + this.props.id).modal('close')
    }

    showadd = e => {
        e.preventDefault()
        window.$("#addmodal" + this.props.id).modal('open')
    }

    onAddChange = e => {
        let value = e.target.value
        this.setState({
            ...this.state,
            add: value
        })

    }


    render() {
        return (

            <div className={`input-field col s${this.props.s} m${this.props.m} ${this.props.containerclass}`}>
                <input id={this.props.id} disabled={this.props.disabled || false} onKeyUp={this.validateInput} onKeyPress={this.validateInput} className={(this.props.className || '') + ((this.props.isvalid == null || typeof (this.props.isvalid) === 'undefined') ? '' : (this.props.isvalid ? 'valid' : 'invalid'))} type={this.props.type || 'text'} placeholder={this.props.placeholder} value={this.props.value} name={this.props.name} onChange={this.props.onChange} style={this.props.useaddbtn ? { width: "90%" } : {}} />
                <label htmlFor={this.props.id} className={this.props.value ? "active" : null}>{this.props.label}</label>
                {this.props.helper ? (<span className="helper-text" data-error={this.props.errortext} data-success={this.props.successtext}>{this.props.helpertext}</span>) : null}
                {this.props.useaddbtn ? (<i className="material-icons prefix" onClick={this.showadd} style={{ fontSize: "1.8rem", lineHeight: "3rem", color: "#42a5f5" }}>add</i>) : null}
                {this.props.useaddbtn ?
                    (
                        <div id={"addmodal" + this.props.id} className="modal" style={{ overflowY: "unset" }}>
                            <div className="modal-content" style={{ paddingBottom: "3px" }}>
                                <div className="row nomargin">
                                    <div className={`input-field col s12 m12 nomargin`}>
                                        <input id={this.props.id + 'add'} disabled={this.props.disabled || false}
                                            className={"nomargin" + ((this.props.isvalid == null || typeof (this.props.isvalid) === 'undefined') ? '' :
                                                (this.props.isvalid ? 'valid' : 'invalid'))} type={this.props.type || 'text'}
                                            placeholder={this.props.placeholder} value={this.state.add}
                                            onChange={this.onAddChange} />
                                        < label htmlFor={this.props.id + 'add'} className={this.props.value ? "active" : null}>{Common.formatUang(this.props.value) + " +"}</label>
                                    </div>
                                </div>
                            </div>
                            <div className="modal-footer center-align" style={{ paddingTop: "2px" }}>
                                <div className="row">
                                    <div className="col s12">
                                        <button disabled={this.state.add ? false : true} className="btn btn-success fill" onClick={this.onAdd}><i className="material-icons">check</i></button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    ) : null
                }


            </div>
        )
    }

}


export default Input