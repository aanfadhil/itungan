import React, { Component } from 'react';

const $ = window.$

class ModalUtil extends Component {
    componentDidMount() {
        $('#modalutil').modal({
            endingTop: '50%',
            startingTop: '5vh'
        })
    }

    open = () => {
        $('#modalutil').modal('open')
    }



    render() {

        let footer;
        if (this.props.type === 'confirm') {
            footer = (<div className="row">
                <div className="col 12 right">
                    <button className="btn-small" style={{ marginRight: "10px" }}>Yes</button>
                    <button className="btn-small">No</button>
                </div>
            </div>)
        }
        else if (this.props.type === 'info') {
            footer = (<div className="row">
                <div className="col 12 right">
                    <button className="btn-small" onClick={this.props.onok}>OK</button>
                </div>
            </div>)
        }

        return (
            <div id="modalutil" className="modal" style={{ overflowY: "unset" }}>
                <div className="modal-content" style={{ paddingBottom: "3px" }}>
                    {this.props.children}
                </div>
                <div className="modal-footer center-align" style={{ paddingTop: "2px" }}>
                    {footer}
                </div>
            </div>
        )
    }

}


export default ModalUtil


export class ModalClose extends Component {

    onClose = e => {
        e.preventDefault()
        
        if (this.props.onClose && typeof (this.props.onClose) === 'function') this.props.onClose(e);

        $('#' + this.props.popupid).modal('close')
    }

    render() {
        return (
            <i className="material-icons grey-text" style={{ position: "absolute", top: "7px", right: "7px", fontSize: "1.2rem" }} onClick={this.onClose}>close</i>
        )
    }
}