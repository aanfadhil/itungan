import axios from 'axios';
import Accounting from './accounting'
import moment from 'moment'
import Decimal from 'decimal.js';
import store from './store'
import { setLoading } from './actions/utilAction'

//const baseURL = 'http://localhost:5000/api/'
//const baseURL = 'http://192.168.1.67:5000/api/'
const baseURL = 'https://itungan.com/api/'

moment.locale('id');

export const Common = {
    formatUang: function (value) {
        return Accounting.formatMoney(value, 'Rp ', 0, '.', ',')
    },
    toDecimal: function (value) {
        return Accounting.unformat(value, ',')
    },
    dateToString: (date, format) => {
        return moment(date).format(format)
    },
    isNumberOrEmpty: (value) => {
        let result = true

        if (value) {
            result = !isNaN(value)
        }

        return result
    },
    validateNumberInput: (value) => {
        let result = ''

        if (value) {
            result = !isNaN(value) ? '' : 'invalid'
        }

        return result
    },
    validateEmail: email => {
     if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))
      {
        return (true)
      }
        //alert("You have entered an invalid email address!")
        return (false)
    }
}



export const Api = {
    Get: function (url, data) {
        let auth = localStorage.getItem('.a')

        let headers = {
            'Content-Type': 'application/json'
        }

        try {
            let token = JSON.parse(auth).token
            if (token) headers.Authorization = 'bearer ' + token
        }
        catch (err) {

        }

        return axios({
            method: 'get',
            url: baseURL + url,
            headers,
            params: data || null
        })
    },

    Post: function (url, data, params) {
        let auth = localStorage.getItem('.a')

        let headers = {
            'Content-Type': 'application/json'
        }

        store.dispatch(setLoading(true))

        try {
            let token = JSON.parse(auth).token
            if (token) headers.Authorization = 'bearer ' + token
        }
        catch (err) {

        }

        return new Promise(
            function (resolve, reject) {
                axios({
                    method: 'post',
                    url: baseURL + url,
                    headers,
                    params: params || null,
                    data: data || null
                })
                    .then(res => {
                        store.dispatch(setLoading(false))
                        resolve(res)
                    })
                    .catch(err => {
                        store.dispatch(setLoading(false))
                        reject(err)
                    })


            }
        )
    },
    PostForm: function (url, data, opt) {
        store.dispatch(setLoading(true))
        let auth = localStorage.getItem('.a')

        opt = opt || {}
        opt.headers = opt.headers || {}
        // let headers = {
        //     'Content-Type': 'application/json'
        // }

        try {
            let token = JSON.parse(auth).token
            if (token) opt.headers.Authorization = 'bearer ' + token
        }
        catch (err) {

        }

        return new Promise(
            function (resolve, reject) {
                axios({
                    method: 'post',
                    url: baseURL + url,
                    headers: opt.headers,
                    params: opt.params || null,
                    data: data || null
                }).then(res => {
                    store.dispatch(setLoading(false))
                    resolve(res)
                })
                    .catch(err => {
                        store.dispatch(setLoading(false))
                        reject(err)
                    })

            }
        )
    }
}

Array.prototype.sum = function (prop) {

    if (typeof (prop) === 'string') {

        var total = 0
        for (var i = 0, _len = this.length; i < _len; i++) {
            total += parseInt(this[i][prop])
        }
        return total
    }
    else if (typeof (prop) === 'function') {
        var total = 0
        for (var i = 0, _len = this.length; i < _len; i++) {
            total += new Decimal(prop(this[i])).toNumber()
        }

        return total
    }
}
