import React, { Component } from 'react';
import { Provider } from 'react-redux';
import store from './store';
import { BrowserRouter as Router } from 'react-router-dom';
import ControllerRegister from './ControllerRegister';



class App extends Component {

  componentDidMount(){
    window.onerror = function (msg, url, lineNo, columnNo, error) {
      alert(msg+'\n##'+url+'\n##'+lineNo)
    }
  }

  render() {
    return (
      <Router>
        <Provider store={store}>
          <ControllerRegister/>
        </Provider>
      </Router>
    );
  }
}

export default App;
