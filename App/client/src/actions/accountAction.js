import { ACCOUNT } from './types';
// import axios from 'axios';
import { Api } from '../Helper'

export const googleLogin = (idToken, callback) => dispatch => {
    Api.Post('users/validatetoken', { idToken })
        .then(res => {

            localStorage.setItem('.a', JSON.stringify({
                token: res.data.token,
                profile: res.data,
            }))

            callback(res.data)

            dispatch({
                type: ACCOUNT.LOGIN,
                payload: {
                    token: res.data.token,
                    profile: res.data,
                    access: []
                }
            })
        })


};

export const register = (user, callback) => dispatch => {
    Api.Post('users/register', { ...user })
        .then(res => {

            localStorage.setItem('.a', JSON.stringify({
                token: res.data.token,
                profile: res.data,
            }))

            callback(res.data)

            dispatch({
                type: ACCOUNT.LOGIN,
                payload: {
                    token: res.data.token,
                    profile: res.data,
                    access: []
                }
            })
        })


};

export const login = (user, callback) => dispatch => {
    Api.Post('users/login', { ...user })
        .then(res => {
            if (res.data.status === 'success') {
                localStorage.setItem('.a', JSON.stringify({
                    token: res.data.data.token,
                    profile: res.data.data,
                }))

                dispatch({
                    type: ACCOUNT.LOGIN,
                    payload: {
                        token: res.data.token,
                        profile: res.data,
                        access: []
                    }
                })

                callback(res.data)

                
            }
            else{
                callback(res.data)
            }
        })


};

export const getAuth = (callback) => dispatch => {

    let user = (localStorage.getItem('.a') || '').trim()

    if (user) {

        let userData = JSON.parse(user)

        dispatch({
            type: ACCOUNT.GETAUTH,
            payload: {
                token: userData.token,
                profile: userData.profile,
                access: [],
                authChecked: true
            }
        })

        return true
    }
    else {
        dispatch({
            type: ACCOUNT.GETAUTH,
            payload: {
                token: '',
                profile: null,
                access: [],
                authChecked: true
            }
        })

        return false
    }
};


export const logout = () => dispatch => {

    localStorage.removeItem('.a')

    dispatch({
        type: ACCOUNT.LOGOUT,
        payload: {
            token: '',
            profile: null,
            access: []
        }
    })

};

