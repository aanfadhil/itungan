
export const GET_ITEMS = 'GET_ITEMS';
export const ADD_ITEM = 'ADD_ITEM';
export const DELETE_ITEM = 'DELETE_ITEM';
export const ITEMS_LOADING = 'ITEMS_LOADING';


export const BAYAR = {
    GET_DETAIL : 'GET_DETAIL',
    GET_LIST : 'GET_LIST',
    GET_BY_ID : 'GET_BY_ID',
    SAVE : 'SAVE',
    BAYAR : 'BAYAR',
    UPLOAD : 'UPLOAD'
}


export const UTIL = {
    LOADING : 'LOADING'
}


export const ACCOUNT = {
    LOGIN : 'LOGIN',
    LOGOUT:'LOGOUT',
    GETAUTH:'GETAUTH',
}