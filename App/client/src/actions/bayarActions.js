import { BAYAR } from './types';
import { Api } from '../Helper'
import {setLoading} from './utilAction'

export const getDetail = (id) => dispatch => {
    //dispatch(setItemsLoading());
    dispatch({
        type: BAYAR.GET_DETAIL,
        payload: {
            total: 330000,
            pajakp: 10,
            pajakrp: 30000,
            title: 'Nongkrong Nemenin Orang Galau '+id,
            bayarbersama : [
                {item : 'Kentang goreng' , total: 16000},
                {item : 'Service charge' , total: 50000},
                {item : 'Room chargezz' , total: 30000},
            ],
            totalbayarbareng : 96000
        }
    })
}

export const getList = () => dispatch => {
    //dispatch(setLoading(true))

    Api.Post('bss/mybsslist',null)
    .then((res) => {
        dispatch({
            type: BAYAR.GET_LIST,
            payload: res.data || []
        })
        dispatch(setLoading(false))
    }).catch((err)=>{
        //console.log(err)
        dispatch({
            type: BAYAR.GET_LIST,
            payload: []
        })

        dispatch(setLoading(false))
    })
}

export const getById = (id,callback) => dispatch => {
    //dispatch(setLoading(true))

    Api.Post('bss/getbyid',{id})
    .then((res) => {
        dispatch({
            type: BAYAR.GET_BY_ID,
            payload: res.data
        })

        
        callback(res)

        dispatch(setLoading(false))
    }).catch((err)=>{
        dispatch({
            type: BAYAR.GET_BY_ID,
            payload: {
                total: 0,
                pajakp: 0,
                pajakrp: 0,
                title: '',
                bayarbersama: [
                ],
                totalbayarbareng: 0
            }
        })

        dispatch(setLoading(false))
    })
}

export const getBayarData = (id,callback) => dispatch => {
    //dispatch(setLoading(true))

    Api.Post('bss/getbayardata',{id})
    .then((res) => {
        dispatch({
            type: BAYAR.GET_BY_ID,
            payload: res.data
        })

        
        callback(res)

        dispatch(setLoading(false))
    }).catch((err)=>{
        dispatch({
            type: BAYAR.GET_BY_ID,
            payload: {
                total: 0,
                pajakp: 0,
                pajakrp: 0,
                title: '',
                bayarbersama: [
                ],
                totalbayarbareng: 0
            }
        })

        dispatch(setLoading(false))
    })
}

export const save = (data,callback) => dispatch => {
    //dispatch(setLoading(true))

    //console.log(JSON.stringify(data))
    Api.Post('bss/upsert',data)
    .then((res) => {
        dispatch({
            type: BAYAR.SAVE
        })
        callback(res)
        dispatch(setLoading(false))
    }).catch((err) =>{
        console.log(err)
        dispatch(setLoading(false))
    })
}

export const uploadImage = (data,id,callback) => dispatch => {
    //dispatch(setLoading(true))

    //console.log(JSON.stringify(data))
    Api.PostForm('bss/upload_photos?id='+id,data)
    .then((res) => {
        dispatch({
            type: BAYAR.UPLOAD
        })
        callback(res)
        dispatch(setLoading(false))
    }).catch((err) =>{
        console.log(err)
        dispatch(setLoading(false))
    })
}

export const bayar = (data,callback) => dispatch => {
    //dispatch(setLoading(true))
    data.items.forEach(element => {
        element.pajak = element.pajak || null;
    });
    //console.log(JSON.stringify(data))
    Api.Post('bss/bayar',data)
    .then((res) => {
        dispatch({
            type: BAYAR.BAYAR
        })
        callback(res)
        dispatch(setLoading(false))
    }).catch((err) =>{
        console.log(err)
        dispatch(setLoading(false))
    })
}