import { GET_ITEMS, ADD_ITEM, DELETE_ITEM, ITEMS_LOADING } from './types';
import axios from 'axios';

export const getItems = () => dispatch => {
    dispatch(setItemsLoading());
    axios.get('/api/items')
        .then(result => {
            console.log(result);
            dispatch({
                type: GET_ITEMS,
                payload: result.data
            })
        })
};

export const addItem = item => dispatch => {
    console.log(item);
    axios.post('/api/items',item)
        .then(result => {
            console.log(result);
            dispatch({
                type: ADD_ITEM,
                payload: result.data
            })
        })

    return {
        type: ADD_ITEM,
        payload: item
    };
};


export const deleteItem = id => dispatch => {

    axios
        .delete(`/api/items/${id}`)
        .then(res => {
            dispatch({
                type:DELETE_ITEM,
                payload:id
            });
        })

    return {
        type: DELETE_ITEM,
        payload: id
    };
};

export const setItemsLoading = () => {
    return {
        type: ITEMS_LOADING,
    };
};