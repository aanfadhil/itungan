import React, { Component } from 'react'
import { Switch } from 'react-router-dom'
import Route from 'react-router-dom/Route'

import HomeController from './controller/HomeController'
import AccountController from './controller/AccountController'
import BSSController from './controller/BSSController'
import IuranController from './controller/IuranController'

class ControllerRegister extends Component {

    render() {
        return (
            <div>
                <Switch>
                    <Route path="/login" component={AccountController} />
                    <Route path="/register" component={AccountController} />
                    <Route path="/account" component={AccountController} />

                    <Route path="/bss" component={BSSController} />

                    <Route path="/iuran" component={IuranController} />

                    <Route path="/home" component={HomeController} />
                    <Route path="/" exact component={HomeController} />
                    <Route path="" exact component={HomeController} />

                </Switch>
            </div>
        )
    }

}

export default ControllerRegister
