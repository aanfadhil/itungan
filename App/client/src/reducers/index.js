import {combineReducers} from 'redux';
import itemReducer from './itemReducer';
import bayarReducer from './bayarReducer';
import accountReducer from './accountReducer';
import utilReducer from './utilReducer';

export default combineReducers({
    item:itemReducer,
    bayar:bayarReducer,
    account:accountReducer,
    util:utilReducer
})