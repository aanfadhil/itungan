import { ACCOUNT } from '../actions/types'

const initialState = {
    token: '',
    profile: null,
    access: [],
    authChecked:false
}

export default function (state = initialState, action) {
    switch (action.type) {
        case ACCOUNT.LOGIN:
            
            return {
                ...state,
                token: action.payload.token,
                profile: action.payload.profile,
                access: action.payload.access
            }

        case ACCOUNT.LOGOUT:
            console.log(state)
            return {
                ...state,
                token: '',
                profile: null,
                access: []
            }

        case ACCOUNT.GETAUTH:
            return {
                ...state,
                ...action.payload
            }

        default:
            return state;
    }
}