import { BAYAR } from '../actions/types'

const initialState = {
    bayar: {
        total: 0,
        pajakp: 0,
        pajakrp: 0,
        title: '',
        bayarbersama: [
        ],
        totalbayarbareng: 0
    },
    bsses: []
}
export default function (state = initialState, action) {
    switch (action.type) {
        case BAYAR.GET_DETAIL:
            return {
                ...state,
                bayar: action.payload
            }

        case BAYAR.GET_LIST:
            return {
                ...state,
                bsses: action.payload
            }
        case BAYAR.GET_BY_ID:
            return {
                ...state,
                bayar:action.payload
            }
        default:
            return state;
    }
}