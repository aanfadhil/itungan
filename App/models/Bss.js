var shortid = require('shortid');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
const fs = require('fs')
const path = require('path');

const BayarBersamaSchema = new Schema({
    _id: { type: String, default: shortid.generate },
    title: String,
    nilai: Number,
    pajak: Number
}, { id: false })

const ItemSchema = new Schema({
    nama: String,
    nilai: Number,
    jumlah: Number,
    pajak: Number
})

const ParticipantsSchema = new Schema({
    email: String,
    nama: String,
    uangdikeluarkan: Number,
    items: [ItemSchema]
})

BayarBersamaSchema.set('toObject', { getters: true });
BayarBersamaSchema.set('toJSON', { getters: true });

const BssSchema = new Schema({
    _id: { type: String, default: shortid.generate },
    title: String,
    total: Number,
    potongan: Number,
    pajakp: Number,
    pajakrp: Number,
    struk: String,
    bersama: [BayarBersamaSchema],
    link: String,
    owner: String,
    participantCount: Number,
    participants: [ParticipantsSchema],
    status: { type: String, default: "open" },
    createdat: { type: Date, default: Date.now }
}, { id: false })

BssSchema.set('toObject', { getters: true });
BssSchema.set('toJSON', { getters: true });

module.exports.Bss = Bss = mongoose.model('bss', BssSchema);

module.exports.Upsert = (item, callback) => {

    if (!item._id) {
        item._id = shortid.generate()

        item.link = '/bss/' + encodeURIComponent(item._id)
        item.createdat = Date.now()
    }

    Bss.findOneAndUpdate({ _id: item._id }, item, { upsert: true }, (err, res) => {
        callback(err, item);
    })

}

module.exports.GetByEmail = (email, callback) => {
    Bss.find({ $or:[ { participants: { $elemMatch: { email: email } } }, { owner:email }  ] }).sort({ createdat: 'ascending' }).exec(callback)
}

module.exports.GetById = (id, email, callback) => {
    Bss.findById(id, (err, result) => {
        if (!err && result) {
            //if (result.owner == email || true) { //sementara bypass
            callback(err, result)
            // }
            // else {
            //     callback("unautorized", null)
            // }
        }
        else {
            callback(err, null)
        }
    })
}



module.exports.getPublicById = (id, callback) => {
    Bss.findById(id, (err, result) => {
        if (!err && result) {
            //if (result.owner == email || true) { //sementara bypass
            callback(err, result)
            // }
            // else {
            //     callback("unautorized", null)
            // }
        }
        else {
            callback(err, null)
        }
    })
}


module.exports.upsertparticipants = (item, id, callback) => {

    Bss.findById(id, (err, result) => {
        if (!err && result) {
            let bss = result.toJSON()

            bss.participants = [
                {
                    email: item.email,
                    nama: item.nama,
                    uangdikeluarkan: item.uangdikeluarkan,
                    items: (item.items||[]).map(t => {
                        return {
                            nama: t.nama,
                            nilai: t.nilai,
                            jumlah: t.jumlah,
                            pajak : t.pajak
                        }
                    })
                },
                ...bss.participants.filter(t => t.email != item.email)
            ]

            Bss.findOneAndUpdate({ _id: bss._id }, bss).exec((err, res) => {
                callback(err, bss)
            })

        }
        else {
            callback(err, null)
        }
    })

}

module.exports.upsertStruk = (filename, id, callback) => {

    Bss.findById(id, (err, result) => {

        if (!err && result) {
            let bss = result.toJSON()
            let oldfile = bss.struk
            bss.struk = filename

            Bss.findOneAndUpdate({ _id: id }, bss).exec((err, res) => {

                fs.unlink(path.join(process.cwd(), 'uploads/' + oldfile), err => {
                    console.log(err)
                });

                callback(err, bss)
            })

        }
        else {
            callback(err, null)
        }
    })

}