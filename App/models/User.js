const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken')
//create schema 

const UserSchema = new Schema({
    email: String,
    password: {
        type: String,
        required: true
    },
    fullName: String,
    profilePict: String,
    firebaseUid: String,
    authtime: Number,
    token: String
})
UserSchema.set('toObject', { getters: true });
UserSchema.set('toJSON', { getters: true });


module.exports.User = User = mongoose.model('user', UserSchema);

module.exports.createUser = (newUser, callback) => {

    User.find({ email: newUser.email }, (err, res) => {
        if (res.length <= 0) {
            bcrypt.genSalt(10, (err, salt) => {
                bcrypt.hash(newUser.password, salt, (err, hash) => {
                    newUser.password = hash;

                    let token = jwt.sign({ email: newUser.email, name: newUser.nama }, process.env.JWTSECRET);

                    let obj = new User({
                        email: newUser.email,
                        fullName: newUser.nama,
                        profilePict: null,
                        password: newUser.password,
                        token,
                        authtime: Date.now()
                    })
                    obj.save((err2, user) => {
                        callback(err2, obj.toJSON())
                    })
                })
            })
        }
        else {
            callback({ message: 'email sudah terdaftar' }, null)
        }
    })


}

module.exports.login = (user, callback) => {

    User.find({ email: user.email }, (err, res) => {
        if (res.length > 0) {
            let found = res[0]
            bcrypt.compare(user.password, found.password, function (err, isMatch) {
                if (err) {
                    return cb(err, null);
                }

                if(!isMatch){
                    return callback({ message: 'user atau password salah' },null)
                }
                else{
                    let token = jwt.sign({ email: found.email, name: found.nama }, process.env.JWTSECRET);

                    User.findOneAndUpdate({email:found.email},{token},(err2,res2)=>{

                    })

                    return callback(null,{...found.toJSON(),token})

                    
                }
                
            });
        }
        else {
            callback({ message: 'user atau password salah' }, null)
        }
    })


}

module.exports.googlelogin = (decodedToken, callback) => {

    let token = jwt.sign({ email: decodedToken.email, name: decodedToken.name }, process.env.JWTSECRET);
    let data = {
        email: decodedToken.email,
        fullName: decodedToken.name,
        profilePict: decodedToken.picture,
        firebaseUid: decodedToken.uid,
        authtime: decodedToken.auth_time,
        token: token
    }
    User.findOneAndUpdate({ email: decodedToken.email }, data, { upsert: true }, (err, user) => {
        callback(err, {
            email: decodedToken.email,
            fullName: decodedToken.name,
            profilePict: decodedToken.picture,
            authtime: decodedToken.auth_time,
            token: token
        })
    })
}

