const express = require('express');
const router = express.Router();

const { body, validationResult } = require('express-validator/check');
const { sanitizeBody } = require('express-validator/filter');
const User = require('../../models/User');
const auth = require('../../middlewares/auth')

var admin = require("firebase-admin");

var serviceAccount = require("../../config/itungansvc-firebase-adminsdk-dnfx2-1b9b486b8b.json");

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://itungansvc.firebaseio.com"
});

router.post('/validatetoken', (req, res) => {

    admin.auth().verifyIdToken(req.body.idToken)
        .then(function (decodedToken) {

            User.googlelogin(decodedToken, (err, user) => {
                if (err) {
                    res.status(500)
                    res.send({ message: "internal server error" })
                }
                else {
                    res.status(200)
                    res.send({ ...user })
                }
            })
        }).catch(function (error) {

        });

})

router.post('/', auth.apiauth, (req, res) => {

    res.send(req.userauth)

});

router.post('/login',
    [
        body('email').trim().isEmail(),
        body('password').isLength({ min: 6 })
    ],
    (req, res) => {
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            return res.status(200).json({ errors: errors.array() });
        }

        User.login(req.body,(err,result)=>{
            if(err){
                res.status(200).json({status:'failed',error:err})
            }
            else{
                res.status(200).json({status:'success',data:result})
            }
        })
    }
)

router.post('/register',
    [
        body('nama').optional({ nullable: true }).escape().trim(),
        body('email').trim().isEmail(),
        body('password').isLength({ min: 6 }),
        body('passwordcnf').isLength({ min: 6 })
    ],
    (req, res) => {
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            return res.status(422).json({ errors: errors.array() });
        }

        if (req.body.password !== req.body.passwordcnf) return res.status(400).json({ errors: [{ msg: "password tidak sesuai" }] });

        User.createUser(req.body, (err, result) => {
            if (err) {
                return res.status(200).json({ errors: [{ msg: err.message }] })
            }
            else {
                res.status(200)
                res.send({ ...result })
            }
        })

    })

module.exports = router;

