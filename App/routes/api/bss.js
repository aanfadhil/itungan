const express = require('express');
const router = express.Router();
const { body, validationResult } = require('express-validator/check');
const { sanitizeBody } = require('express-validator/filter');
const Bss = require('../../models/Bss');
const url = require('url');
const path = require('path');
const fs = require('fs')
const formidable = require('formidable')
const readChunk = require('read-chunk')
const fileType = require('file-type')
const auth = require('../../middlewares/auth')

router.get('/xxx/:id',(req,res)=>{
    res.json({message:'okokok',id:req.params.id})
})  

//download foto struk
router.get('/downloadstruk/:id',(req,res)=>{
    //res.json({message:'okokok',id:req.params.id})

    //res.download(path.join(process.cwd(),'uploads/1542865912936-DZo8kXKMr.jpg'),'DZo8kXKMr.jpg')

    Bss.getPublicById(req.params.id,(err,result)=>{
        res.download(path.join(process.cwd(),'uploads/'+result.struk),result.struk)
    })

})  

router.post('/downloadstruk/:id',(req,res)=>{
    //res.json({message:'okokok',id:req.params.id})

    res.download(path.join(process.cwd(),'uploads/1542865912936-DZo8kXKMr.jpg'),'KMr.jpg')

    // Bss.getPublicById(req.params.id,(err,result)=>{
    //     res.download(process.cwd(),'uploads/'+result.struk)
    // })

})  

//get list bss
router.post('/mybsslist', auth.getauth, (req, res) => {

    if (!req.userauth) return res.json([])

    Bss.GetByEmail(req.userauth.email, (err, result) => {
        res.json(result || [])
    })
})

//upsert bss baru
//return link
router.post('/upsert',
    auth.apiauth,
    [
        sanitizeBody('title').escape().trim(),
        body('total').optional({ nullable: true }).isNumeric(),
        body('potongan').optional({ nullable: true }).isNumeric(),
        body('pajakp').optional({ nullable: true }).isNumeric(),
        body('pajakrp').optional({ nullable: true }).isNumeric(),
        body('participants.*.email').optional({ nullable: true }).isEmail(),
        body('bersama.*.title').escape().trim(),
        body('bersama.*.nilai').trim().isNumeric(),
        body('participantCount').isNumeric(),
    ],
    (req, res) => {

        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            return res.status(422).json({ errors: errors.array() });
        }

        if (!req.body._id) {
            // req.body.email = req.userauth.email
            // req.body.owner = req.body.email

            // req.body.participants = req.body.participants || []

            // let foundOwner = false
            // for (var i = 0; i < req.body.participants.length; i++) {
            //     if (req.body.participants[i].email == req.body.email) {
            //         foundOwner = true;
            //         break;
            //     }
            // }

            //if (!foundOwner) req.body.participants.push({ email: req.body.email, nama: req.userauth.nama, items: [] })

            Bss.Upsert(req.body, (err, result) => {
                if (!err)
                    res.status(201).json(result)
                else
                    res.status(500).json(err)
            })

        }
        else {


            let foundOwner = false
            for (var i = 0; i < req.body.participants.length; i++) {
                if (req.body.participants[i].email == req.body.email) {
                    foundOwner = true;
                    break;
                }
            }

            if ((req.body.participants||[]).filter(t => t.email == req.userauth.email).length <= 0) req.body.participants.push({ email: req.userauth.email, nama: req.userauth.name, items: [] })


            Bss.GetById(req.body._id, req.userauth.email, (err, result) => {
                if (result.owner !== req.userauth.email) {
                    res.status(401).json({ message: 'unauthorized' })
                }
                else {
                    return Bss.Upsert(req.body, (err, result) => {
                        if (!err)
                            res.status(201).json(result)
                        else
                            res.status(500).json(err)
                    })
                }

            })
        }




    });

//get bss by id
router.post('/getbyid', auth.apiauth, (req, res) => {

    Bss.GetById(req.body.id, req.userauth.email, (err, result) => {
        if (err) {
            console.log(err)
            res.status(200)
            res.send(err)
        }
        else if (result) {
            let fulllink = 'https://itungan.com' + result.link
            res.status(200).json({ ...result.toJSON(), fulllink })
        }
        else {
            res.status(200).json({})
        }
    })

});

router.post('/getbayardata', (req, res) => {

    Bss.getPublicById(req.body.id, (err, result) => {
        if (err) {
            console.log(err)
            res.status(200)
            res.send(err)
        }
        else if (result) {

            let fulllink = 'https://itungan.com' + result.link
            res.status(200).json({ ...result.toJSON(), fulllink })
        }
        else {
            res.status(200).json({})
        }
    })

});

//tutup pembayaran
router.post('/close', auth.apiauth, (req, res) => {


});


//upsert participants waktu input pembayarannya
router.post('/bayar',
    auth.apiauth,
    [
        body('uangkeluar').optional({ nullable: true }).isNumeric(),
        body('items').optional({ nullable: true }).isArray(),
        body('items.*.title').optional({ nullable: true }).escape().trim(),
        body('items.*.nilai').optional({ nullable: true }).trim().isNumeric(),
        body('items.*.jumlah').optional({ nullable: true }).trim().isNumeric(),
        body('items.*.pajak').optional({ nullable: true }).trim().isNumeric()
    ],
    (req, res) => {

        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            return res.status(422).json({ errors: errors.array() });
        }

        req.body.email = req.userauth.email
        req.body.nama = req.userauth.name

        Bss.upsertparticipants(req.body, req.body._id, (err, result) => {

            if (!err) {
                res.status(200).json(result);
            }
            else {
                res.status(500).json(err);
            }

        })


    });

//upload foto struk
router.post('/upload_photos', auth.apiauth ,function (req, res) {
    var photos = [],
        form = new formidable.IncomingForm();

    // Tells formidable that there will be multiple files sent.
    form.multiples = true;
    // Upload directory for the images


    form.uploadDir = path.join(process.cwd(), 'tmp_uploads');

    // Invoked when a file has finished uploading.
    form.on('file', function (name, file) {
        // Allow only 3 files to be uploaded.
        if (photos.length === 3) {
            fs.unlink(file.path);
            return true;
        }

        var buffer = null,
            type = null,
            filename = '';

        // Read a chunk of the file.
        buffer = readChunk.sync(file.path, 0, 262);
        // Get the file type using the buffer read using read-chunk
        type = fileType(buffer);

        // Check the file type, must be either png,jpg or jpeg
        if (type !== null && (type.ext === 'png' || type.ext === 'jpg' || type.ext === 'jpeg')) {
            let url_parts = url.parse(req.url, true)
            let id = url_parts.query.id
            // Assign new file name
            filename = Date.now() + '-' + id +'.'+type.ext;

            // Move the file with the new file name
            fs.rename(file.path, path.join(process.cwd(), 'uploads/' + filename));

            // Add to the list of photos
            photos.push({
                status: true,
                filename: filename,
                type: type.ext,
                publicPath: 'uploads/' + filename
            });

            Bss.upsertStruk(filename,id,(err,result)=>{
                
            })


        } else {
            photos.push({
                status: false,
                filename: file.name,
                message: 'Invalid file type'
            });

            fs.unlink(file.path);
        }
    });

    form.on('error', function (err) {
        console.log('Error occurred during processing - ' + err);
    });

    // Invoked when all the fields have been processed.
    form.on('end', function () {
        console.log('All the request fields have been processed.');
    });

    // Parse the incoming form fields.
    form.parse(req, function (err, fields, files) {
        let url_parts = url.parse(req.url, true)
            let id = url_parts.query.id
        res.status(200).json({photos,_id :id });
    });
});


module.exports = router;

