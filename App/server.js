const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const path = require('path');
const cookieParser = require('cookie-parser');
const expressValidator = require('express-validator');
var https = require('https')

const fs = require('fs')
// const session = require('express-session');
// const passport = require('passport');
// const LocalStrategy = require('passport-local').Strategy;


//const items = require('./routes/api/items');
const userRoute = require('./routes/api/user');
const bss = require('./routes/api/bss');

require('dotenv').config()

const app = express();

//bosy parser middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended:false }));
app.use(cookieParser());
app.use(expressValidator());


app.use(function (req, res, next) {

    let allowedOrigin = ['http://localhost:3000','http://192.168.1.67:3000','http://localhost:5000','https://itungan.com']

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', allowedOrigin.filter(t => t == req.headers.origin).pop() || null);

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,Authorization,Accept,Accept-Encoding,Host,Referer,Upgrade-Insecure-Requests,User-Agent');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

app.disable('x-powered-by');

//config db
const db = process.env.MONGOURI;

//connect to mongo
mongoose
    .connect(db, { useNewUrlParser: true })
    .then(() => console.log('DB Connected'))
    .catch((err) => console.log(err));

//app.use('/api/items',items);

app.use('/api/users',userRoute);
app.use('/api/bss',bss);

const port = process.env.PORT || 5000;

// https.createServer({
//     key: fs.readFileSync('ca.key'),
//     cert: fs.readFileSync('ca.crt')
//   }, app)
//   .listen(port, function () {
//     console.log(`Example app listening on port ${port}`)
//   })

app.listen(port,() => console.log(`Server started on port ${port}`));