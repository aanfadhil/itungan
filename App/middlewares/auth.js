
const jwt = require('jsonwebtoken')

module.exports.apiauth = (req, res, next) => {

    if (req.headers.authorization && req.headers.authorization.search('bearer ') === 0) {
        let token = req.headers.authorization.split(' ')[1]
        try {
            let user = jwt.verify(token, process.env.JWTSECRET)
            req.userauth = user
            

            next()
        } catch (err){
            setTimeout(function () {
                res.send({message:'Unauthorized'}, 401);
            }, 10000);
        }


    }
    else {
        res.status(401)
        res.send({ message: 'Unauthorized' })
    }

}

module.exports.getauth = (req, res, next) => {

    if (req.headers.authorization && req.headers.authorization.search('bearer ') === 0) {
        let token = req.headers.authorization.split(' ')[1]
        try {
            let user = jwt.verify(token, process.env.JWTSECRET)
            req.userauth = user
            
            next()
        } catch (err){
            next()
        }


    }
    else {
        next()
    }

}